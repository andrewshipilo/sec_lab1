<?php

include "../models/EmployeeRepository.php";

$config = include("../db/config.php");
$db = new PDO($config["db"], $config["username"], $config["password"]);
$employees = new EmployeeRepository($db);


switch($_SERVER["REQUEST_METHOD"]) {
    case "GET":
        $result = $employees->getAll(array(
            "first_name" => $_GET["first_name"],
            "last_name" => $_GET["last_name"],
            "birth_date" => $_GET["birth_date"],
            "hire_date" => $_GET["hire_date"],
            "gender" => $_GET["gender"]
        ));
        break;

    case "POST":
        $result = $employees->insert(array(
            "first_name" => $_POST["first_name"],
            "last_name" => $_POST["last_name"],
            "birth_date" => $_POST["birth_date"],
            "hire_date" => $_POST["hire_date"],
            "gender" => $_POST["gender"]
        ));
        break;

    case "PUT":
        parse_str(file_get_contents("php://input"), $_PUT);

        $result = $employees->update(array(
            "emp_no" => intval($_PUT["emp_no"]),
            "first_name" => $_PUT["first_name"],
            "last_name" => $_PUT["last_name"],
            "birth_date" => $_PUT["birth_date"],
            "hire_date" => $_PUT["hire_date"],
            "gender" => $_PUT["gender"]
        ));
        break;

    case "DELETE":
        parse_str(file_get_contents("php://input"), $_DELETE);

        $result = $employees->remove(intval($_DELETE["emp_no"]));
        break;
}


header("Content-Type: application/json");
echo json_encode($result);

