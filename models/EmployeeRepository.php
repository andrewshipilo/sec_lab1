<?php

include "Employee.php";

class EmployeeRepository {
    
    protected $db;
    
    public function __construct(PDO $db) {
        $this->db = $db;
    }
    
    private function read($row) {
        $result = new Employee();
        $result->emp_no = $row["emp_no"];
        $result->birth_date = $row["birth_date"];
        $result->first_name = $row["first_name"];
        $result->last_name = $row["last_name"];
        $result->gender = $row["gender"] ;
        $result->hire_date = $row["hire_date"];
        return $result;
    }
    
    public function getById($emp_no) {
        $sql = "SELECT * FROM employees WHERE emp_no = :emp_no";
        $q = $this->db->prepare($sql);
        $q->bindParam(":emp_no", $emp_no, PDO::PARAM_INT);
        $q->execute();
        $rows = $q->fetchAll();
        return $this->read($rows[0]);
    }
    
    public function getAll($filter) {
        $first_name = "%" . $filter["first_name"] . "%";
        $last_name = "%" . $filter["last_name"] . "%";
        $birth_date = "%" . $filter["birth_date"] . "%";
        $hire_date = "%" . $filter["hire_date"] . "%";
        $gender = "%" . $filter["gender"] . "%";

        $sql = "SELECT * FROM employees WHERE first_name LIKE :first_name AND last_name LIKE :last_name AND birth_date LIKE :birth_date AND hire_date LIKE :hire_date AND gender LIKE :gender ORDER BY emp_no ASC";
        
        $q = $this->db->prepare($sql);
        $q->bindParam(":first_name", $first_name);
        $q->bindParam(":last_name", $last_name);
        $q->bindParam(":birth_date", $birth_date);
        $q->bindParam(":hire_date", $hire_date);
        $q->bindParam(":gender", $gender);
        $q->execute();
        $rows = $q->fetchAll();

        $result = array();
        foreach($rows as $row) {
            array_push($result, $this->read($row));
        }
        return $result;
    }

    public function insert(array $data)
    {
        $sql = "INSERT INTO employees(birth_date, first_name, last_name, gender, hire_date) VALUES (:birth_date, :first_name, :last_name, :gender, :hire_date)";
        $q = $this->db->prepare($sql);
        $q->bindParam(":birth_date", $data["birth_date"]);
        $q->bindParam(":first_name", $data["first_name"]);
        $q->bindParam(":last_name", $data["last_name"]);
        $q->bindParam(":gender", $data["gender"]);
        $q->bindParam(":hire_date", $data["hire_date"]);
        $q->execute();
        return $this->getById($this->db->lastInsertId());
    }

    public function update($data) {
        $sql = "UPDATE employees SET birth_date = :birth_date, first_name = :first_name, last_name = :last_name, gender = :gender, hire_date = :hire_date WHERE emp_no = :emp_no";
        $q = $this->db->prepare($sql);
        $q->bindParam(":birth_date", $data["birth_date"]);
        $q->bindParam(":first_name", $data["first_name"]);
        $q->bindParam(":last_name", $data["last_name"]);
        $q->bindParam(":gender", $data["gender"]);
        $q->bindParam(":hire_date", $data["hire_date"]);
        $q->bindParam(":emp_no", $data["emp_no"], PDO::PARAM_INT);
        $q->execute();

        return $data;
    }

    public function remove($emp_no) {
        $sql = "DELETE FROM employees WHERE emp_no = :emp_no";
        $q = $this->db->prepare($sql);
        $q->bindParam(":emp_no", $emp_no, PDO::PARAM_INT);
        $q->execute();
        return $emp_no;
    }
}