/*global $ */

$(function() {

    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };

    MyDateField.prototype = new jsGrid.Field({
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },

        itemTemplate: function(value) {
            return new Date(value).toDateString();
        },

        insertTemplate: function(value) {
            return this._insertPicker = $("<input>").datepicker({ defaultDate: new Date() });
        },

        editTemplate: function(value) {
            return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
        },

        insertValue: function() {
            return this._insertPicker.datepicker("getDate").toISOString();
        },

        editValue: function() {
            return this._editPicker.datepicker("getDate").toISOString();
        }
    });

    jsGrid.fields.datePicker = MyDateField;

    $("#jsGrid").jsGrid({
        height: "70%",
        width: "100%",
        filtering: true,
        inserting: true,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,
        deleteConfirm: "Do you really want to delete client?",
        controller: {
            loadData: function (filter) {
                return $.ajax({
                    type: "GET",
                    url: "/jsgrid-php-master/employees/",
                    data: filter
                });
            },
            insertItem: function (item) {
                return $.ajax({
                    type: "POST",
                    url: "/jsgrid-php-master/employees/",
                    data: item
                });
            },
            updateItem: function (item) {
                return $.ajax({
                    type: "PUT",
                    url: "/jsgrid-php-master/employees/",
                    data: item
                });
            },
            deleteItem: function (item) {
                return $.ajax({
                    type: "DELETE",
                    url: "/jsgrid-php-master/employees/",
                    data: item
                });
            }
        },
        fields: [
            { name: "first_name", title: "Name", type: "text", width: 150 },
            { name: "last_name", title: "Surname", type: "text", width: 150 },
            { name: "birth_date", title: "Birth Date", type: "datePicker", width: 150 },
            { name: "hire_date", title: "Hire Date", type: "datePicker", width: 150 },
            { name: "gender", title: "Gender",  type: "select",
                items:[
                    { Name: "", Id: '' },
                    { Name: "Male", Id: 'M' },
                    { Name: "Female", Id: 'F' },
                ],
                valueField: "Id",
                textField: "Name",
                validate: "required",
                width: 150 },
            { type: "control"}
        ]
    });
});
